#ifndef __YUGONG_HPP__
#define __YUGONG_HPP__

#include "yugong-libunwind-support.hpp"

#include <cstdint>

#define yg_error(fmt, ...) fprintf(stdout, "YG ERROR [%s:%d:%s] " fmt,\
        __FILE__, __LINE__, __func__, ## __VA_ARGS__)

namespace yg {
    // Assumed 64-bit word.  This applies for both x64 and AArch64, but not
    // other archs.
    inline uintptr_t _load_word(uintptr_t addr) {
        return *reinterpret_cast<uintptr_t*>(addr);
    }

    inline void _store_word(uintptr_t addr, uintptr_t word) {
        *reinterpret_cast<uintptr_t*>(addr) = word;
    }

    // The representatin of a stack in libyugong.
    struct YGStack {
        uintptr_t sp;
        uintptr_t start;
        uintptr_t size;

        YGStack() {}
        YGStack(uintptr_t sp, uintptr_t start, uintptr_t size)
            : sp(sp), start(start), size(size) {}

        // Allocate a stack.  `size` is the size of the stack.
        static YGStack alloc(uintptr_t size);

        // Deallocate the stack.
        void free();

        // Initialize the stack with a function at the base of the stack.
        // `func` is the address of the function.  The stack-bottom frame is a
        // ROP frame.  `func` must take one integer parameter.
        void init(uintptr_t func);

        // Like `init`, but `func` takes one double parameter.
        void init_double(uintptr_t func);

        void _init_internal(uintptr_t func, uintptr_t adapter);

        inline uintptr_t* _top_buffer() {
            return reinterpret_cast<uintptr_t*>(sp);
        }

        inline void _move_sp(uintptr_t offset) {
            sp += offset;
        }
        
        template<class T>
        inline T* _push_structure() {
            _move_sp(-(sizeof(T)));
            T *ps = reinterpret_cast<T*>(sp);
            return ps;
        }
    };

    // Frame cursor.  It manipulates stack frames of an inactive stack.
    struct YGCursor {
        unw_cursor_t unw_cursor;
        YGStack *stack;
            
        // Construct a frame cursor which operates on `stack`.  `stack` must be
        // inactive (not bound to a thread).  When constructed, the "current
        // frame" is the top frame of the stack.
        YGCursor(YGStack &stack);

        void _init_unw();

        // Move the cursor one frame down.
        void step();
        uintptr_t _cur_pc();
        uintptr_t _cur_sp();
        void _set_cur_pc(uintptr_t new_pc);
        void _set_cur_sp(uintptr_t new_sp);

        // Remove all frames above the current frame.
        void pop_frames_to();

        // Create a ROP frame for `func` above the current frame, also removing
        // all existing frames above the current frame.  After pushing frame,
        // the current frame will be the newly created frame.
        //
        // The function `func` must take one integer parameter as its sole
        // paramter.
        void push_frame(uintptr_t func);

        // Like push_frame, but `func` takes a double parameter instead of
        // integer parameter.
        void push_frame_double(uintptr_t func);

        void _push_frame_internal(uintptr_t func, uintptr_t adapter);

        void _reconstruct_ss_top();

        void _get_reg(unw_regnum_t reg, uintptr_t *out);
        void _get_fpreg(unw_regnum_t reg, unw_fpreg_t *out);
        void _set_reg(unw_regnum_t reg, uintptr_t newval);
    };
}

extern "C" {
    // Synonym of yg_stack_swap_ii
    uintptr_t yg_stack_swap(yg::YGStack *from, yg::YGStack *to, uintptr_t value);

    // This is the swap-stack operation.`to` points to the stack to swap to.
    // `from` should point to an uninitialized variable of `YGStack` type.
    // Register values will be pushed on the current stack, and the stack
    // pointer will be saved in `from` before swapping the stack pointer.
    // `value` will be passed to the top frame of the `to` stack.
    //
    // This version passes an integer value, and receives an integer.
    uintptr_t yg_stack_swap_ii(yg::YGStack *from, yg::YGStack *to, uintptr_t value);

    // This version passes an double value, and receives an integer.
    uintptr_t yg_stack_swap_di(yg::YGStack *from, yg::YGStack *to, double value);

    // This version passes an integer value, and receives an double.
    double yg_stack_swap_id(yg::YGStack *from, yg::YGStack *to, uintptr_t value);

    // This version passes an integer value, and receives an double.
    double yg_stack_swap_dd(yg::YGStack *from, yg::YGStack *to, double value);

    // ASM-implemented service routines. Not real functions.
    // Don't call from C++!
    void _yg_func_begin_resume();
    void _yg_func_begin_resume_double();
    void _yg_stack_swap_cont();
}

#endif // __YUGONG_HPP__
