# libyugong

*Yu Gong is the protagonist in the story of [The Foolish Old Man Removes the
Mountains](https://en.wikipedia.org/wiki/The_Foolish_Old_Man_Removes_the_Mountains)
in Chinese myths.*

`libyugong` is a proof-of-concept project to demonstrate the on-stack
replacement API of the [Mu micro virtual machine](http://microvm.org/) on native
(C/C++) programs.  Mu is a minimal language-neutral VM which offloads most
high-level run-time feedback-directed optimisation to a high-level program (i.e.
a 'client') that uses Mu as an underlying tool.  Mu supports such optimisation
by providing the SWAPSTACK operation and an API that supports stack unwinding,
stack introspection and on-stack replacement.  `libyugong` is created during the
designing phase of Mu to show that the Mu stack API which is based on SWAPSTACK
and return-oriented programming can be practically implemented for compiled
code.

`libyugong` currently supports x64 on OSX and GNU/Linux, and AArch64 on
GNU/Linux.

The main part of `libyugong` contains two parts:

1.  The abstraction of stacks, and an implementation of the SWAPSTACK operation.
2.  A 'frame cursor' which can unwind the stack, remove frames from a stack, and
    add new frames to a stack.  Note that the frame cursor *does not* support
    Mu's stack introspection API.

There is also a component `yugong-llvm`.  It is an attempt to use LLVM's stack
maps implementation as a tool for stack introspection, but is currently not
working. It intended to provide (1) an LLVM helper library that helps LLVM users
generate and manage stackmaps and their metadata at compile time when the user
builds LLVM IR code, and (2) a run-time library that decodes the LLVM stack maps
to support stack introspection, i.e. restoring LLVM IR variable values in a
given stack frame.

# How to build

## Dependencies

`libyugong` is written in C++11, so you need a C++ compiler that supports C++11.
Either GCC or clang should work.  `libyugong` uses `make` for building.

`libyugong` depends on the `libunwind` library provided by LLVM.  It is provided
in the `deps/libunwind` directory of this project.  Building `libunwind`
requires LLVM and CMake 3.4.3.

You need to install those dependencies using the package manager of your
distribution.  If you use ArchLinux, invoking `pacman -S gcc llvm cmake make`
from a fresh installation should be sufficient to install all build dependencies
for `libyugong`.

## Native Compilation

It can be compiled by simply invoking `make` (or using `make -j` to accelerate
building).  The resulting archive `libyugong.a` and sample programs will be
generated in the `target` directory.

## Cross Compilation for AArch64

If you do not have a real `AArch64` device, you can use a cross compiling
toolchain to compile `libunwind` and `libyugong`, and use QEMU to run AArch64
programs.  The installation depends on your system.  The following example uses
the ArchLinux distribution on x64.

Install the cross compiling toolchain:

```
sudo pacman -S aarch64-linux-gnu-{binutils,gcc,glibc}
```

Set environment variables that affects the compilation:

```
export CC=$(which aarch64-linux-gnu-gcc)
export CXX=$(which aarch64-linux-gnu-g++)
export AS=$(which aarch64-linux-gnu-as)
export LD=$(which aarch64-linux-gnu-ld)
```

Then invoke `make`.  You may want to change the `TARGET` variable so that the
AArch64 output files co-exists with your x64 output files:

```
make -j TARGET=target-aarch64
```

Install QEMU for AArch64:

```
sudo pacman -S qemu-headless qemu-headless-arch-extra
```

Execute an example program using `qemu-aarch64`:

```
export QEMU_LD_PREFIX=/usr/aarch64-linux-gnu
export QEMU_SET_ENV=LD_LIBRARY_PATH=/usr/aarch64-linux-gnu/lib64
qemu-aarch64 target-aarch64/test_chop_push_return 4
```

# Examples

The `tests` directory contains many example programs, all of which have
`test_*.cpp` as their names.  They will be compiled into executable files in the
target directory.

There are several interesting examples:

-   `test_swapstack`: Demonstrate coroutines and the swap-stack operation.

-   `test_example1`: Demonstrate a stack of normal frames.

-   `test_example2`: Demonstrate a stack of hand-crafted ROP frames.

-   `test_example3`: Like example 2, but uses floating point numbers.

-   `test_chop_return`: Demonstrate the removal of stack frames.

-   `test_chop_push_return`: Demonstrate the replacement (removal and creation)
    of stack frames.

Read the comments at the top of their source files for more information.

# Concepts

This library uses the *swap-stack* operation to rebind a thread to a different
stack.  The original stack is stopped at the point of the swap-stack operation,
and can be resumed later to continue after the swap-stack operation.  When a
thread is swapped away from a stack using the swap-stack operation, every frame
on the inactive stack is stopped at a *resumption point*, the point where it
continues after the frame is resumed.

This library uses [return-oriented
programming (ROP)](https://en.wikipedia.org/wiki/Return-oriented_programming) to
construct new frames on top of existing frames.  A frame is a *normal frame* if
its resumption point is a call site (including the call to the swap-stack
operation).  A frame is a *ROP frame* if the resumption point is the entry point
of the function.  When a frame is resumed, a normal frame receives the return
value as the value of the function call expression of the call site, but a ROP
frame receives the return value as its parameter.

The API of this library allows the user to remove frames from the top of a
stack, and construct ROP frames on the top of the frame.

# API

The programming interface is defined in `yugong/yugong.hpp`.

When using this library, the user creates new stacks using the `YGStack::alloc`
static method.  The user should then create the bottom frame of the stack using
the `YGStack::init` method.  Stacks can be destroyed with the `YGStack::free`
method.

The `yg_stack_swap` performs the swap-stack operation and passes a value to the
top frame of the destination stack.  There are several variants of swap-stack:

*   `yg_stack_swap`: Synonym of `yg_stack_swap_ii`.

*   `yg_stack_swap_ii`: Pass an integer (any integer type), and receive an
    integer.

*   `yg_stack_swap_id`: Pass integer, receive double.

*   `yg_stack_swap_di`: Pass double, receive integer.

*   `yg_stack_swap_dd`: Pass double, receive double.

Most functionalities depend on the frame cursor `YGCursor`, which operates on a
inactive (not bound to any thread) stack.

*   The `step` method moves the cursor one frame down.

*   The `pop_frames_to` removes all frames above the current frame, leaving the
    current frame on the top of the stack.

*   The `push_frame` creates a ROP frame of a given function on the top of the
    current frame (implies removing all frames above). The function must have
    one integer parameter as its sole parameter.

*   The `push_frame_double` is like `push_frame`, but the frame takes a double
    as its parameter.

The user can also use the private `_cur_pc` and `_cur_sp` method to get the
current program counter and stack pointer address for debug purpose.

# Implementation Details

This library depends on the application binary interface (ABI) of the system,
including the calling convention and the stack unwinding information.  We also
depend on the ABI-compliant C/C++ compiler.

The key to the implementation includes swap-stack, stack unwinding, and the
construction of ROP frames.  Several important functions are written in the
assembly language in `yg_asm.S`.

## Swap-stack

The swap-stack operation `yg_stack_swap` is implemented in assembly in
`yg_asm.S`.  It uses the C calling convention to make the caller save
caller-saved registers, while itself saves callee-saved registers on the top of
the stack.  The concrete stack structure after saving registers is
architecture-specific.  When an inactive stack is resumed, the program continues
with the `_yg_stack_swap_cont` routine which restores the saved registers.

## Stack unwinding

We use `libunwind` to unwind the stack, during which the callee-saved registers
of frames below the top frame will be restored by `libunwind`.  After removing
or creating frames, we reconstruct the stack-top structure expected by
`_yg_stack_swap_cont` which contains saved callee-saved register, so that the
swap-stack operation can always resume from the stack-top frame with
callee-saved registers holding the appropriate values of the resumption point.

## ROP frames

The `YGCursor::push_frame` method creates ROP frames.  The challenge of creating
ROP frames is different on different architectures.

On x64, the main difficulty is that the integer return value is passed via the
`RAX` register, while the first integer parameter is expected to be in the `RDI`
register.  Therefore, for every ROP frame, we push two addresses on the stack,
with the address of the desired function below the address of the adapter code
`_yg_func_begin_resume`.  When returning to a ROP frame, it will first return to
the adapter.  The adapter code on x64 moves the integer value from `RAX` to
`RDI` before returning to the desired function.  If the ROP frame receives a
floating point (double) value, the return value is already in `XMM0`, the same
register as the parameter.  But we use a trivial adapter
(`_yg_func_begin_resume_double`), too, to maintain 16-byte stack pointer
alignment.

On AArch64, the return value is passed in the same register as the first
parameter.  However, the main difficulty is that we can only construct frames by
pushing values on the stack, but the return address is expected in the `X30`
register (LR).  Therefore, when we construct a ROP frame, we remember the
current value of the `X30` register which is the PC the new ROP frame will
return to after it returns.  We push the current `X30` and the address of the
desired function on the stack, and replace the current `X30` register with the
address of the adapter code `_yg_func_begin_resume`.  The adapter on AArch64
loads both the saved `X30` and the address of the desired function from the ROP
frame, restores the `X30` register, and branches to the desired function.

## Limitation

This is a proof-of-concept project in C.  In this library, ROP frames and the
swap-stack operation can receive or pass one integer or one double value.  This
is as much as we can do in C.  In theory, the swap-stack operation can pass
multiple values, and functions can return multiple values, too.  Multiple return
values need to be demonstrated in a different language.

This library depends on the system ABI, and is not assisted by a compiler.
Therefore, we cannot perform stack introspection, which is usually required for
real-world feedback-directed optimisation during on-stack replacement.  In
theory, if we use an ABI-compliant compiler, such as LLVM, and generate
stack maps, we will be able to retrieve language-level local variable values.

Without compiler assistance, this library is also vulnerable to function
inlining.  If a function is inlined, the frame cursor will not be able to decode
a physical frame into logical frames that match the high-level language
semantics.  Handling inlining requires the compiler to generate stack maps at
call sites.

# License

The MIT license. See [LICENSE](LICENSE).

# Author

Kunshan Wang <kunshan.wang@anu.edu.au>


<!--
vim: tw=80 spell spelllang=en_au
-->
