/** This example is like test_example2, except it uses double as the parameter
 * and return value type.
 */

#include <yugong.hpp>
#include "test_common.hpp"

#include <cinttypes>
#include <cstdint>
#include <cstdio>

using namespace yg;

YGStack main_stack, coro_stack;

int plus_one(double x) {
    ygt_print("my return addr is %p\n", __builtin_return_address(0));
    ygt_print("my frame ptr is %p\n", __builtin_frame_address(0));

    ygt_print("x = %lf\n", x);

    int rv = (int)x + 1;
    ygt_print("rv = %d\n", rv);

    return rv;
}

double times_two(int y) {
    ygt_print("my return addr is %p\n", __builtin_return_address(0));
    ygt_print("my frame ptr is %p\n", __builtin_frame_address(0));

    ygt_print("y = %d\n", y);

    double rv = y * 2.0;
    ygt_print("rv = %lf\n", rv);

    return rv;
}

void print(double z) {
    ygt_print("my return addr is %p\n", __builtin_return_address(0));
    ygt_print("my frame ptr is %p\n", __builtin_frame_address(0));

    ygt_print("z = %lf\n", z);
    printf("%lf\n", z);

    return;
}

void coro_base_func() {
    ygt_print("my return addr is %p\n", __builtin_return_address(0));
    ygt_print("my frame ptr is %p\n", __builtin_frame_address(0));

    ygt_print("Swapping back to main stack...\n");
    yg_stack_swap(&coro_stack, &main_stack, 0);
}

int main(int argc, char *argv[]) {
    coro_stack = YGStack::alloc(4096);
    coro_stack.init((uintptr_t)coro_base_func);

    ygt_print("Debug information:\n");
    ygt_print("  coro_stack.begin = %" PRIxPTR "\n", coro_stack.start);
    ygt_print("  coro_stack.size  = %" PRIxPTR "\n", coro_stack.size);
    ygt_print("  coro_stack ends at %" PRIxPTR "\n", coro_stack.start + coro_stack.size);
    ygt_print("  coro_base_func = %p\n", coro_base_func);
    ygt_print("  plus_one       = %p\n", plus_one);
    ygt_print("  times_two      = %p\n", times_two);
    ygt_print("  print          = %p\n", print);
    ygt_print("  _yg_func_begin_resume = %p\n", _yg_func_begin_resume);
    ygt_print("\n");
    ygt_print("Constructing ROP frames...\n");

    YGCursor cursor(coro_stack);

    ygt_print("Pushing frame for print\n");
    cursor.push_frame_double(reinterpret_cast<uintptr_t>(print));
    ygt_print("Pushing frame for times_two\n");
    cursor.push_frame(reinterpret_cast<uintptr_t>(times_two));
    ygt_print("Pushing frame for plus_one\n");
    cursor.push_frame_double(reinterpret_cast<uintptr_t>(plus_one));

    const double VALUE = 42.0;
    ygt_print("Now, swap stack to coroutine stack, passing value: %lf\n", VALUE);

    int result2 = static_cast<int>(yg_stack_swap_di(&main_stack, &coro_stack, VALUE));

    ygt_print("Back to main stack.  Received value (from coro_base_func): %d\n", result2);

    coro_stack.free();

    return 0;
}
