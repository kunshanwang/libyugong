/** This example demonstrates coroutines.
 *
 * Unlike conventional C/C++ programs, the control flow in this program jumps
 * between two coroutines that have their own control stacks.  A swap-stack
 * operation stops the current stack, and resumes the other stack, which
 * implements the control flow of coroutines.
 */

#include <yugong.hpp>
#include "test_common.hpp"

#include <cstdint>
#include <cstdio>

using namespace yg;

YGStack main_stack, coro_stack;

void sbf(uintptr_t arg) {
    char *name = (char*)arg;
    ygt_print("Hello '%s' from coro!\n", name);

    // (2)

    double c2 = yg_stack_swap_id(&coro_stack, &main_stack, 25L);
    ygt_print("c2 = %lf\n", c2);

    // (4)

    long c3 = yg_stack_swap_di(&coro_stack, &main_stack, 75.0);
    ygt_print("c3 = %ld\n", c3);

    // (6)

    yg_stack_swap_ii(&coro_stack, &main_stack, 125);
}

int main(int argc, char *argv[]) {
    coro_stack = YGStack::alloc(4096);
    coro_stack.init((uintptr_t)sbf);

    const char *name = "world";

    ygt_print("Hello '%s' from main!\n", name);

    // (1)

    long m1 = yg_stack_swap(&main_stack, &coro_stack, reinterpret_cast<uintptr_t>(name));
    ygt_print("m1 = %ld\n", m1);

    // (3) 

    double m2 = yg_stack_swap_dd(&main_stack, &coro_stack, 50.0);
    ygt_print("m2 = %lf\n", m2);

    // (5)

    int m3 = yg_stack_swap_ii(&main_stack, &coro_stack, 100L);
    ygt_print("Welcome back.  The last message m3 = %d\n", m3);

    coro_stack.free();

    return 0;
}
