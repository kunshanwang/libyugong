/** This example demonstrates a stack of hand-crafted ROP frames.
 *
 * The function coro_base_func sits at the base of the coroutine stack.
 * However, before we swap to the coroutine stack, we create several ROP frames
 * on top of coro_base_func.  From bottom to top, we push frames for print,
 * times_two and plus_one.
 *
 * At this moment, the coroutine stack consists of frames for (from top to
 * bottom) plus_one, times_two, print, and coro_base_func, each stopped at the
 * entry point of each function.  When the main function performs swap-stack,
 * passing the value 42, the following things will happen:
 *
 * 1. The top frame for function plus_one is resumed, receiving the value 42
 * from its parameter x.
 *
 * 2. After plus_one returns, the next frame for times_two is resumed, receiving
 * the return value 43 from its parameter y.
 *
 * 3. After times_two returns, the next frame for print is resumed, receiving
 * the return value 86 from its parameter z.
 *
 * 4. The print function prints 86, and returns to coro_base_func.
 *
 * 5. coro_base_func performs a swap-stack operation to the main stack, which
 * terminates the program.
 *
 * This control flow is unusual.  The return value of each frame is received by
 * the parameter of the next frame.  The functions of the frames are
 * independent, because no function called the function of the frame above it.
 * This unusual return-oriented programming (ROP) "paradigm" allows us to
 * construct arbitrary frames upon existing frames as long as the return type
 * matches the expected type.  This is the basis of our on-stack replacement
 * mechanism.
 *
 * This example should be compared with test_example1.cpp, which demonstrates a
 * stack of normal frames created by ordinary function calls.
 */

#include <yugong.hpp>
#include "test_common.hpp"

#include <cinttypes>
#include <cstdint>
#include <cstdio>

using namespace yg;

YGStack main_stack, coro_stack;

int plus_one(int x) {
    ygt_print("my return addr is %p\n", __builtin_return_address(0));
    ygt_print("my frame ptr is %p\n", __builtin_frame_address(0));

    ygt_print("x = %d\n", x);

    int rv = x + 1;
    ygt_print("rv = %d\n", rv);

    return rv;
}

int times_two(int y) {
    ygt_print("my return addr is %p\n", __builtin_return_address(0));
    ygt_print("my frame ptr is %p\n", __builtin_frame_address(0));

    ygt_print("y = %d\n", y);

    int rv = y * 2;
    ygt_print("rv = %d\n", rv);

    return rv;
}

void print(int z) {
    ygt_print("my return addr is %p\n", __builtin_return_address(0));
    ygt_print("my frame ptr is %p\n", __builtin_frame_address(0));

    ygt_print("z = %d\n", z);
    printf("%d\n", z);

    return;
}

void coro_base_func() {
    ygt_print("my return addr is %p\n", __builtin_return_address(0));
    ygt_print("my frame ptr is %p\n", __builtin_frame_address(0));

    ygt_print("Swapping back to main stack...\n");
    yg_stack_swap(&coro_stack, &main_stack, 0);
}

int main(int argc, char *argv[]) {
    coro_stack = YGStack::alloc(4096);
    coro_stack.init((uintptr_t)coro_base_func);

    ygt_print("Debug information:\n");
    ygt_print("  coro_stack.begin = %" PRIxPTR "\n", coro_stack.start);
    ygt_print("  coro_stack.size  = %" PRIxPTR "\n", coro_stack.size);
    ygt_print("  coro_stack ends at %" PRIxPTR "\n", coro_stack.start + coro_stack.size);
    ygt_print("  coro_base_func = %p\n", coro_base_func);
    ygt_print("  plus_one       = %p\n", plus_one);
    ygt_print("  times_two      = %p\n", times_two);
    ygt_print("  print          = %p\n", print);
    ygt_print("  _yg_func_begin_resume = %p\n", _yg_func_begin_resume);
    ygt_print("\n");
    ygt_print("Constructing ROP frames...\n");

    YGCursor cursor(coro_stack);

    ygt_print("Pushing frame for print\n");
    cursor.push_frame(reinterpret_cast<uintptr_t>(print));
    ygt_print("Pushing frame for times_two\n");
    cursor.push_frame(reinterpret_cast<uintptr_t>(times_two));
    ygt_print("Pushing frame for plus_one\n");
    cursor.push_frame(reinterpret_cast<uintptr_t>(plus_one));

    const long VALUE = 42;
    ygt_print("Now, swap stack to coroutine stack, passing value: %ld\n", VALUE);

    int result2 = static_cast<int>(yg_stack_swap(&main_stack, &coro_stack, VALUE));

    ygt_print("Back to main stack.  Received value (from coro_base_func): %d\n", result2);

    coro_stack.free();

    return 0;
}
