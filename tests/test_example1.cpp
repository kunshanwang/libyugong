/** This example demonstrates a normal stack created by function calls.
 *
 * The function coro_base_func represents the entry point of a coroutine, which
 * is executed on a separate stack.  coro_base_func calls foo, foo calls bar,
 * bar calls baz, and baz calls moo.  moo performs a swap-stack operation back
 * to the main stack.
 *
 * At this moment, the coroutine stack consists of frames for (from top to
 * bottom) moo, baz, bar, foo and coro_base_func, each stopped at the line with
 * the "stop here" comment.  When the main function performs another swap-stack
 * operation, it resumes the stack-top function moo, and the passed value is
 * received by the "rv" variable.  The coroutine stack will proceed as usual.
 * Eventually, the coro_base_func will swap back to the main stack, where the
 * program ends.
 *
 * This example should be compared with test_example2.cpp, which demonstrates a
 * stack of hand-crafted ROP frames.
 */

#include <yugong.hpp>
#include "test_common.hpp"

#include <cinttypes>
#include <cstdint>
#include <cstdio>

using namespace yg;

YGStack main_stack, coro_stack;

long moo() {
    ygt_print("my return addr is %p\n", __builtin_return_address(0));
    ygt_print("my frame ptr is %p\n", __builtin_frame_address(0));

    ygt_print("Preparing to swap stack...\n");
    long rv = (long)yg_stack_swap(&coro_stack, &main_stack, 0); // stop here

    return rv;
}

long baz() {
    ygt_print("my return addr is %p\n", __builtin_return_address(0));
    ygt_print("my frame ptr is %p\n", __builtin_frame_address(0));

    ygt_print("Preparing to call moo()...\n");
    long x = moo();     // stop here
    ygt_print("x = %ld\n", x);

    return x;
}

double bar() {
    ygt_print("my return addr is %p\n", __builtin_return_address(0));
    ygt_print("my frame ptr is %p\n", __builtin_frame_address(0));

    ygt_print("Preparing to call baz()...\n");
    long x = baz();     // stop here
    ygt_print("x = %ld\n", x);

    double y = (double)(x + 1);
    ygt_print("y = %lf\n", y);

    return y;
}

int foo() {
    ygt_print("my return addr is %p\n", __builtin_return_address(0));
    ygt_print("my frame ptr is %p\n", __builtin_frame_address(0));

    ygt_print("Preparing to call bar()...\n");
    double y = bar();   // stop here
    ygt_print("y = %lf\n", y);

    int z = printf("%lf\n", y);
    ygt_print("z = %d\n", z);

    return z;
}

void coro_base_func() {
    ygt_print("Preparing to call foo()...\n");
    int result = foo(); // stop here
    ygt_print("result = %d\n", result);
    
    yg_stack_swap(&coro_stack, &main_stack, result);
}

int main(int argc, char *argv[]) {
    coro_stack = YGStack::alloc(4096);
    coro_stack.init((uintptr_t)coro_base_func);

    ygt_print("Debug information:\n");
    ygt_print("  coro_stack.begin = %" PRIxPTR "\n", coro_stack.start);
    ygt_print("  coro_stack.size  = %" PRIxPTR "\n", coro_stack.size);
    ygt_print("  coro_stack ends at %" PRIxPTR "\n", coro_stack.start + coro_stack.size);
    ygt_print("  coro_base_func = %p\n", coro_base_func);
    ygt_print("  foo            = %p\n", foo);
    ygt_print("  bar            = %p\n", bar);
    ygt_print("  baz            = %p\n", baz);
    ygt_print("  moo            = %p\n", moo);
    ygt_print("\n");
    ygt_print("Swapping stack...\n");

    int result = static_cast<int>(yg_stack_swap(&main_stack, &coro_stack, 0));

    ygt_print("Back to main stack.  Received value (from moo): %d\n", result);

    const long VALUE = 42;
    ygt_print("Now, swap stack again, passing value: %ld\n", VALUE);

    int result2 = static_cast<int>(yg_stack_swap(&main_stack, &coro_stack, VALUE));

    ygt_print("Back to main stack.  Received value (from coro_base_func): %d\n", result2);

    coro_stack.free();

    return 0;
}
